# Unix Commands

- [awk](#awk)
    * [extract text between tags](#extract-text-between-tags)
- [grep](#grep) 
    * [grep between two tags](#grep-between-two-tags) 
    * [list imported packages](#list-imported-packages) 
- [kubectl](#kubectl)
    * [export svc definitions to yaml files](#export-svc-definitions-to-yaml-files)
- [rename](#rename)
    * [rename file or folder names](#rename-file-or-folder-names)
- [sed](#sed)
    * [replace file contents in directory](#replace-file-contents-in-directory)
- - - -

### awk

- #### extract text between tags

  example:
  ```bash
  $ cat jboss-web.xml
  <?xml version="1.0" encoding="UTF-8"?>
  <jboss-web version="8.0" xmlns="http://www.jboss.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <context-root>/dispatch</context-root>
  </jboss-web>
  $ 
  $ 
  $ awk -vRS="</context-root>" '/<context-root>/{gsub(/.*<context-root>|\n+/,"");print;exit}' jboss-web.xml #may need replace empty spaces
  /dispatch
  $ 
  $ 
  $ awk -vRS="</context-root>" '/<context-root>/{gsub(/.*<context-root>|\n +/,"");print;exit}' jboss-web.xml #replaces empty spaces
  /dispatch
  ```

- - - -

### grep

- #### grep between two tags

  example:
  ```bash
  $ cat jboss-web.xml
  <?xml version="1.0" encoding="UTF-8"?>
  <jboss-web version="8.0" xmlns="http://www.jboss.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <context-root>/dispatch</context-root>
  </jboss-web>
  $ 
  $ 
  $ grep -P -o -e '(?<=<context-root>)(.*)(?=<\/context-root>)' jboss-web.xml
  /dispatch
  ```
  
- #### list imported packages

  ```bash
  $ grep '^import ' *.java | sed -e 's/.*import *//' -e 's/;.*$//' | sort -u >list
  ```

- - - -

### kubectl

- #### export svc definitions to yaml files

  ```bash
  $ for p in $(kubectl get svc -n namespace-name -o name |  sed -e 's/.*\///g'); do kubectl get svc -n namespace-name $p -o yaml >> $p.yaml; done
  ```

- - - -

### rename

- #### rename file or folder names

  ```bash
  $ #rename folders
  $ find -name "*OldName*" -type d -exec rename OldName NewName '{}' \;
  $ #rename files
  $ find -name "*OldName*" -type f -exec rename OldName NewName '{}' \;
  $ #rename using for loop
  $ for p in $(find -name "*OldName*" -type d); do rename OldName NewName '{}' $p; done
  $ for p in $(find -name "*OldName*" -type f); do rename OldName NewName '{}' $p; done
  ```

- - - -

### sed

- #### replace file contents in directory

  ```bash
  $ for file in $(find -type f); do sed -e "s/OldString/NewString/g" -e "s/AnotherStringToReplace/NewStringType2/g" $file > $file; done
  ```
